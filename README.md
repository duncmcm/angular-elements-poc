# ElementsApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Some useful resources

<https://medium.com/@neckster/how-to-build-real-web-components-with-angular-6-ac842c9062dd>

<https://angular.io/guide/elements>

<https://angularfirebase.com/lessons/angular-elements-quick-start-guide/>

<https://angularfirebase.com/lessons/angular-elements-advanced-techniques/>