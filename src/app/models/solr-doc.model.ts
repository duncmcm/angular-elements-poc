export interface SolrDoc {
    id: string;
    tour_code_s: string;
    url_s: string;
    dep_s: string;
    num_days_l: number;
    tour_title_s: string;
    visits: string[];
    description: string[];
    months: string[];
    departure_time: string[];
    return_time: string[];
    ordprc_min_l: number;
    concprc_min_l: number;
    ordprc_max_l: number;
    concprc_max_l: number;
    curr_s: string;
    _version_: number;
}