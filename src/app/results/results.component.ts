import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  OnChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  ApplicationRef
} from '@angular/core';
import { SolrService } from '../solr.service';
import { SolrDoc } from '../models/solr-doc.model';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnChanges {
  @Input() public dp: string;
  results: SolrDoc[] = [];
  constructor(private solrService: SolrService, private applicationRef: ApplicationRef, private cdr: ChangeDetectorRef) {}

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.solrService.solrData(changes.dp.currentValue).subscribe(data => {
      console.log(data);
      this.results = data;
      this.cdr.markForCheck();
    });
  }
  check() {
    console.log('component view checked');
  }
}
