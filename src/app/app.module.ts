import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import {createCustomElement} from '@angular/elements';
import { DepPointsComponent } from './dep-points/dep-points.component';
import { ResultsComponent } from './results/results.component';
import { HttpClientModule } from '@angular/common/http'; 

@NgModule({
  declarations: [
    DepPointsComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: [DepPointsComponent, ResultsComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap() {
    const dpEl = createCustomElement(DepPointsComponent, {injector: this.injector});
    customElements.define('dep-list', dpEl);
    const rEl = createCustomElement(ResultsComponent, {injector: this.injector});
    customElements.define('results-list', rEl);
  }
}
