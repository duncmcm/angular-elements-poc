import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SolrService {
  constructor(private httpClient: HttpClient) {}
  solrData(depPoint: string = '*') {
    return this.httpClient
      .get(`http://192.168.3.42:8983/solr/en/select?indent=on&q=dep_s:${depPoint.toUpperCase()}*&wt=json`)
      .pipe(
        map((data: any) => {
          console.log(data.response.docs);
          return data.response.docs;
        })
      );
  }
}
