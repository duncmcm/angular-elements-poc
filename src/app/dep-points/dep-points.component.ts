import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, ChangeDetectorRef, ApplicationRef } from '@angular/core';
import { of, from, Observable, Subject } from 'rxjs';
import { map, delay, mergeMap, switchMap, concatMap } from 'rxjs/operators';
import { diPublicInInjector } from '@angular/core/src/render3/di';

@Component({
 // encapsulation: ViewEncapsulation.Native, // This needs to be disableed for IE compatiblity - look into alternatives/polyfills
  selector: 'app-dep-points',
  templateUrl: './dep-points.component.html',
  styleUrls: ['./dep-points.component.css']
})
export class DepPointsComponent implements OnInit {

  @Output() depPointText = new EventEmitter<string>();
  depList: string[] = ['Edinburgh', 'Glasgow', 'Dublin', 'London', 'Inverness'];
  currentDepPoint: string;
  dpSubject = new Subject<string>();
  dp$ = this.dpSubject.asObservable();
  constructor(private cdr: ChangeDetectorRef) {}


  itemClicked(depPoint): void {
      this.depPointText.emit(depPoint);
      console.log(depPoint);
      this.currentDepPoint = depPoint;
      this.dpSubject.next(depPoint);
      console.log(this.dp$);
      this.cdr.markForCheck();
  }
  ngOnInit() {
      this.dp$.subscribe((data)=>{console.log(data)});
   }
}
