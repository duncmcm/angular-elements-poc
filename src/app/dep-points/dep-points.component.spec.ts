import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepPointsComponent } from './dep-points.component';

describe('DepPointsComponent', () => {
  let component: DepPointsComponent;
  let fixture: ComponentFixture<DepPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
