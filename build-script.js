const fs = require('fs-extra');
const concat = require('concat');    
const copyfiles = require('copyfiles');
const replace = require('replace-in-file');
(async function build() {

    const files =[
        
        './dist/elementsApp/runtime.js',
        './dist/elementsApp/es2015-polyfills.js',
        './dist/elementsApp/polyfills.js',
        './dist/elementsApp/main.js',
        './dist/elementsApp/scripts.js'
    ]
    
    await fs.ensureDir('elements')

    await concat(files, 'elements/dep-list.js');
    copyfiles(['./dist/elementsApp/index.html', './elements'], 2, (blah)=>{
        console.log('Copied index.html');
        const str = 'foo';
        const regex = new RegExp('((<\\bscript\\b).*</\\bscript>)+');
        const options = {
            files: './elements/index.html',
            from: regex,
            to: '<script src="dep-list.js"></script>',
        };
        try {
            const changes = replace.sync(options);
            console.log('Modified files:', changes.join(', '));
          }
          catch (error) {
            console.error('Error occurred:', error);
          }
    });
    copyfiles(['./dist/elementsApp/styles.css', './elements'], 2, (blah)=>{console.log('Copied css')});
    console.info('Elements created successfully!')

})()